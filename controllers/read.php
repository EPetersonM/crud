<?php 

    include '../models/read.php';

    $respuesta = ModelRead::read();
    $res = "";
    if($respuesta){
        foreach ($respuesta as $key => $item) {
            $res.= '<tr>
                        <th>'.$item["id"].'</th>
                        <th>'.$item["marca"].'</th>
                        <th>'.$item["modelo"].'</th>
                        <th>'.$item["stock"].'</th>
                        <th><a data-toggle="modal" onclick="editIndex('.$item["id"].');" data-target="#modalEdit" class="btn m-0 py-0"><i class="fa fa-pencil text-success"></i></a>
                            <a class="btn m-0 py-0" id="deleteBtn" onclick="del('. $item["id"].');"><i class="fa fa-trash text-danger"></i></a></th>
                    </tr>';
        }
    
        echo $res;
        // echo json_encode($respuesta);
    } else {
        return 'Error';
    }

    
    ?>