<?php

include 'conexion.php';

class ModelCreate{

    public function create($data){

        $stmt = Conexion::Conectar()->prepare("INSERT INTO moviles (marca,modelo,stock) VALUES (:marca,:modelo,:stock)");

        $stmt -> bindParam(":marca", $data['marca'], PDO::PARAM_STR);
        $stmt -> bindParam(":modelo", $data['modelo'], PDO::PARAM_STR);
        $stmt -> bindParam(":stock", $data['stock'], PDO::PARAM_INT);

        if($stmt->execute()){
            return "ok";
        } else {
            return 'error';
        }

        $stmt->close();
    }
}
