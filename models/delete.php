<?php

include 'conexion.php';
class ModelDelete {

    public function delete($id) {
        $stmt = Conexion::Conectar()->prepare("DELETE FROM moviles WHERE id=:id");
        $stmt -> bindParam(":id", $id, PDO::PARAM_INT);
    
        if($stmt->execute()){
            return "ok";
        } else {
            return 'error';
        }
    
        $stmt->close();
    }
}
