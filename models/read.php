<?php

include 'conexion.php';
class ModelRead {
    
    public function read(){
        $stmt = Conexion::Conectar()->prepare("SELECT * FROM moviles");
    
        $stmt -> execute();
    
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    
        $stmt->close();
    
    }

    public function read_by_id($id) {
        $stmt = Conexion::Conectar()->prepare("SELECT * FROM moviles WHERE id = :id");
        $stmt -> bindParam(":id", $id, PDO::PARAM_INT);
    
        if($stmt -> execute() > 0) {
            return $stmt->fetch(PDO::FETCH_ASSOC);
        } else {
            return 'No se encontró el registro';
        }
    
        $stmt->close();
    }

    public function search($data) {
        $data = "%".$_POST["s"]."%";
        $stmt = Conexion::Conectar()->prepare("SELECT * FROM moviles WHERE marca LIKE :data OR modelo LIKE :data");
        $stmt -> bindParam(":data", $data, PDO::PARAM_STR);

        $stmt -> execute();
    
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    
        $stmt->close();
    }
}
