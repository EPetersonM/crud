<?php

class Conexion {
    
    public static function Conectar() {
        define('host','localhost');
        define('database','vuetify_bd');
        define('user','root');
        define('password','');

        $conection = new PDO("mysql:host=".host."; dbname=".database, user, password);
        return $conection;
    }

}