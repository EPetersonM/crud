<?php

include 'conexion.php';

class ModelEdit{

    public function edit($data){

        $stmt = Conexion::Conectar()->prepare("UPDATE moviles SET marca=:marca,modelo=:modelo,stock=:stock WHERE id=:id");

        $stmt -> bindParam(":id", $data["id"], PDO::PARAM_STR);
        $stmt -> bindParam(":marca", $data['marca'], PDO::PARAM_STR);
        $stmt -> bindParam(":modelo", $data['modelo'], PDO::PARAM_STR);
        $stmt -> bindParam(":stock", $data['stock'], PDO::PARAM_INT);

        if($stmt->execute()){
            return "ok";
        } else {
            return 'error';
        }

        $stmt->close();
    }
}
