--
-- Base de datos: `vuetify_bd`
--
DROP DATABASE IF EXISTS `vuetify_bd`;
CREATE DATABASE IF NOT EXISTS `vuetify_bd` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `vuetify_bd`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `moviles`
--

CREATE TABLE `moviles` (
  `id` int(11) NOT NULL,
  `marca` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `modelo` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `stock` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `moviles`
--

INSERT INTO `moviles` (`id`, `marca`, `modelo`, `stock`) VALUES
(1, 'XIAOMI', 'MI 9T PRO', 25),
(3, 'SAMSUNG', 'J2 CORE ', 110),
(7, 'IPHONE', 'XI', 35),
(8, 'HUAWEI', 'P30 PRO', 30),
(12, 'SAMSUNG', 'J2 PRIME', 15),
(18, 'MOTOROLA', 'RZR', 10),
(19, 'SAMSUNG', 'S10 +', 5),
(20, 'SAMSUNG', 'S10', 10),
(26, 'IPHONE', '11', 11);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `moviles`
--
ALTER TABLE `moviles`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `moviles`
--
ALTER TABLE `moviles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
